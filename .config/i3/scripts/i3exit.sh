#!/usr/bin/env sh
case "$1" in
    lock)
        betterlockscreen --lock
        exit 0
    ;;
    logout)
        i3-msg exit
    ;;
    suspend)
        systemctl suspend
    ;;
    reboot)
        systemctl reboot
    ;;
    shutdown)
        systemctl poweroff
    ;;
    *)
        echo "Usage: $0 {lock|logout|suspend|hibernate|reboot|shutdown}"
        exit 2
    ;;
esac

exit 0
