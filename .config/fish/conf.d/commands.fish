function l; ls -lAsh --dereference-command-line -v -t $argv; end
function le; ls -lAsh --dereference-command-line -v -t --color=always $argv | less -r; end
function pof; shutdown -P now; end
function setclip; xclip -selection c; end
function getclip; xclip -selection c -o; end
