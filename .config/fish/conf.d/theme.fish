set -g theme_powerline_fonts no
set -g theme_nerd_fonts yes
set -g theme_git_default_branches master main
set -g theme_display_vagrant yes
set -g theme_display_docker_machine yes
set -g theme_date_format "+%a %H:%M"
set -g theme_date_timezone Europe/Moscow
set -g theme_show_exit_status yes
set -g theme_display_jobs_verbose yes
set -g theme_color_scheme solarized
set -g theme_display_hostname yes
set -g theme_display_user no
