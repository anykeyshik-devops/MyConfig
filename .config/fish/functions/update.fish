function update
    set current_dir (pwd)
    yay -Syyu
    omf update
    fundle update
    cd ~/.tmux; git pull origin master
    cd ~/.SpaceVim; git pull origin master
    cd $current_dir
end
