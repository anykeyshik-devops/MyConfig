function bobthefish_colors -S -d 'Define a custom bobthefish color scheme'

  	# optionally include a base color scheme...
  	__bobthefish_colors solarized

  	# then override everything you want! note that these must be defined with `set -x`
  	set -x fish_color_command                 ffffff
  	set -x fish_color_autosuggestion          444444
 	set -x fish_color_cancel                  normal
  	set -x fish_color_comment                 87af00
  	set -x fish_color_cwd                     008000
  	set -x fish_color_cwd_root                800000
  	set -x fish_color_end                     005fff
  	set -x fish_color_error                   ff0000
  	set -x fish_color_escape                  00a6b2
	set -x fish_color_history_current         normal
 	set -x fish_color_host_remote	          yellow
    set -x fish_color_match	                  normal
    set -x fish_color_normal	              normal
    set -x fish_color_operator	              00a6b2
    set -x fish_color_param	                  33CCCC
    set -x fish_color_quote	                  5CCCCC
    set -x fish_color_redirection	          BF7130
    set -x fish_color_search_match	          ffff00
    set -x fish_color_selection	              c0c0c0
    set -x fish_color_status	              red
    set -x fish_color_user	                  00ff00
    set -x fish_color_valid_path	          normal
end
