if status is-interactive
	  # Fundle init
	  fundle plugin 'docker/cli' --path 'contrib/completion/fish'
    fundle plugin 'brgmnn/fish-docker-compose'
	  fundle plugin 'danhper/fish-ssh-agent'
	  fundle init

	  # Set env vars
	  my_fish_sourcenv $HOME/.env/global.env
end
# Start X at login
if status is-login
    if test -z "$DISPLAY" -a "$XDG_VTNR" = 1
        exec startx -- -keeptty
    end
end
