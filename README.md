# Pack of my configs

## Tmux
[Oh my tmux](https://github.com/gpakosz/.tmux)


## Fish
[Oh my fish](https://github.com/oh-my-fish/oh-my-fish) with [fundle](https://github.com/danhper/fundle) and [bobthefish](https://github.com/oh-my-fish/theme-bobthefish) theme


## Vim
[SpaceVim](https://spacevim.org/)


## GDB
[GEF](https://github.com/hugsy/gef)


## Yay for pacman
[Yay](https://github.com/Jguer/yay)


## Git
`.gitconfig` and `.gitignore_global` files


## Nerd fonts
[Nerd fonts](https://github.com/ryanoasis/nerd-fonts)


## Firefox
userChrome.css for Firefox
* _Disalbe Ctrl-q exit_ `browser.quitShortcut.disabled = true`
* _Enable userChrome_ `toolkit.legacyUserProfileCustomizations.stylesheets = true`


## SystemTweaks
Scripts for tweak system


## Programs in aliases:
* gping
* dust
* duf
* git
* NeoVim
* xclip
